var gantt_arr = [];
gantt_arr.push({goal_id: 'goal-A', calendar_id: '2018-6-21', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 4});
gantt_arr.push({goal_id: 'goal-A', calendar_id: '2018-6-20', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 4});
gantt_arr.push({goal_id: 'goal-B', calendar_id: '2018-6-24', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 3, denominator_hours: 4});
gantt_arr.push({goal_id: 'goal-B', calendar_id: '2018-6-19', week_id: '2018-wk15', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 3});
gantt_arr.push({goal_id: 'goal-C', calendar_id: '2018-6-19', week_id: '2018-wk16', quarter_id: '2018-q2', numerator_hours: 2, denominator_hours: 3});

function filterGantt(test_gantt_arr, goal_id){
	var filtered_gantt = [];
	gantt_arr.map((item, index)=>{
		if(item.goal_id===goal_id){
			filtered_gantt.push(item);
		} 
	});	
	return filtered_gantt;
}

function sortGantt(gantt_arr){
		var sorted_gantt = gantt_arr.sort(compareSort);
		return sorted_gantt;
}

function compareSort(a, b) {
	a_arr = a.calendar_id.split('-');
	b_arr = b.calendar_id.split('-');
  if (a_arr[0]<b_arr[0]) {
    return -1;
  }
  if (a_arr[0]===b_arr[0]&&a_arr[1]<b_arr[1]) {
    return -1;
  }
  if (a_arr[0]===b_arr[0]&&a_arr[1]===b_arr[1]&&a_arr[2]<b_arr[2]) {
    return -1;
  }
  if (a_arr[0]>b_arr[0]) {
    return 1;
  }
  if (a_arr[0]===b_arr[0]&&a_arr[1]>b_arr[1]) {
    return 1;
  }
  if (a_arr[0]===b_arr[0]&&a_arr[1]===b_arr[1]&&a_arr[2]>b_arr[2]) {
    return 1;
  }
  // a must be equal to b
  return 0;
}

function parseGantt(gantt_arr, goal_id){
	var filtered_gantt = filterGantt(gantt_arr, goal_id);
	var sorted_filtered_gantt = sortGantt(filtered_gantt);
	var input_arr = [];
	var initial_calendar_id = null;
	var sub_arr = [];
	sorted_filtered_gantt.map((item, index)=>{
		if(initial_calendar_id===null){
			initial_calendar_id = item.calendar_id;
			input_arr.push(1);
			return;
		}
		var days = timeBetweenDays(initial_calendar_id, item.calendar_id);
		sub_arr = new Array(days).fill(0);
		sub_arr.push(1);
		input_arr = input_arr.concat(sub_arr);
		initial_calendar_id = item.calendar_id;
		return;
	});
	return input_arr;
}

function timeBetweenDays(date1, date2){
	return 0;
}

console.log(parseGantt(gantt_arr, 'goal-A'));
