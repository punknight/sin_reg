var assert = require('assert');
var date_calc = require('./date_calc.js');

describe('date_calc', function(){
	describe('constructor', function(){
		it('should create date_calc object by parsing string', function(done){
			var date_str = '2018-0-31';
			var date_obj1 = new date_calc(date_str);
			assert(date_obj1.date[0]==2018);
			assert(date_obj1.date[1]===0);
			assert(date_obj1.date[2]==31);
			done();
		});
		it('should create date_calc object by parsing arr', function(done){
			var date_arr = [1985, 10, 28];
			var date_obj2 = new date_calc(date_arr);
			assert(date_obj2.date[0]==1985);
			assert(date_obj2.date[1]==10);
			assert(date_obj2.date[2]==28);
			done();
		});
		it('should create date_calc object by parsing js Date()', function(done){
			var new_date1 = new Date(2018, 0, 1);
			var date_obj3 = new date_calc(new_date1);
			assert(date_obj3.date[0]==2018);
			assert(date_obj3.date[1]===0);
			assert(date_obj3.date[2]==1);
			done();
		});
		it('should create date_calc object by parsing js Date()', function(done){
			var new_date2 = new Date();
			var date_obj4 = new date_calc();
			console.log(date_obj4.date[0]+' '+new_date2.getFullYear());
			assert(date_obj4.date[0]==new_date2.getFullYear());
			assert(date_obj4.date[1]==new_date2.getMonth());
			assert(date_obj4.date[2]==new_date2.getDate());
			done();
		});
	});
	describe('date_comparer', function(){
		it('should prove date1 > date2', function(done){
			var date1 = new date_calc([2018, 0, 2]);
			var date2 = new date_calc([2018, 0, 1]);
			var compare_test1 = date1.date_comparer(date2.date);
			var compare_test2 = date2.date_comparer(date1.date);
			var compare_test3 = date1.date_comparer(date1.date);
			assert(compare_test1===1);
			assert(compare_test2===-1);
			assert(compare_test3===0);
			done();
		});
	});
	describe('date_adder', function(){
		it('should add expression to the date', function(done){
			var date3 = new date_calc([2018, 0, 31]);
			var new_date3 = date3.date_adder([0, 0, 1]);
			assert(new_date3.date[0]===2018);
			assert(new_date3.date[1]===1);
			assert(new_date3.date[2]===1);
			done();
		});
	});
	describe('date_subtractor', function(){
		it('should find the difference between dates', function(done){
			var date1 = new date_calc([2018, 0, 31]);
			var date2 = new date_calc([2018, 1, 1]);
			
			var new_expression = date1.date_subtractor(date2.date);
			
			assert(new_expression[0]===0);
			assert(new_expression[1]===0);
			assert(new_expression[2]===1);
			done();
		});
	})
	
});

