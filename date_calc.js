//test get month 
//test Date constructor
//both go by month index
function date_calc(arg=new Date()){
	switch(typeof arg){
		case 'string':
			var str_arr = arg.split('-');
			this.date = str_arr.map((item)=>{
				return parseInt(item)
			});
			break;
		case 'object':
			if(arg instanceof Array){
				this.date = arg;
				break;	
			}
			if(arg instanceof Date){
				var year = arg.getFullYear();
				var month = arg.getMonth();
				var day = arg.getDate();
				this.date = [year, month, day];
				break;	
			}
			throw 'Parameter is not useable!';
			break;
		default: 
			throw 'Parameter is not useable!';
	}
};

date_calc.prototype.toString = function(){
	function displayMonth(month){
    switch(month){
      case 0:
        return 'January';
        break;
      case 1:
        return 'February';
        break;
      case 2:
        return 'March';
        break;
      case 3:
        return 'April';
        break;
      case 4:
        return 'May';
        break;
      case 5:
        return 'June';
        break;
      case 6:
        return 'July';
        break;
      case 7: 
        return 'August';
        break;
      case 8:
        return 'September';
        break;
      case 9:
        return 'October';
        break;
      case 10:
        return 'November';
        break;
      case 11:
        return 'December';
        break;
      default:
        return 'error';
    }
  }
  var date_str = displayMonth(parseInt(this.date[1]))+' '+this.date[2]+', '+this.date[0];
  return date_str;

}

date_calc.prototype.toDate = function(){
  return new Date(this.date[0], this.date[1], this.date[2]);
}

date_calc.prototype.date_adder = function(date_expression){
	function addDates(date, date_expression){
    var year = date[0] + date_expression[0];
    var month = date[1] + date_expression[1];
    if (month>12) {
      month = month - 12;
      year = year + 1;
    }
    var day = date[2];
    var augend_date = new Date(year, month, day);
    var addend_days = date_expression[2];
    var sum_date = new Date(augend_date.setTime(augend_date.getTime()+addend_days*86400000));
    var date_obj = new date_calc(sum_date)
    return date_obj;
  }
  return addDates(this.date, date_expression);
}

date_calc.prototype.date_subtractor = function(date_arr){
	//need a date comparator
	function subtractDates(date1, date2){
		function daysInMonth (year, month) {
    	return new Date(year, month, 0).getDate();
		}
		function getNextLeapDay(date2){
			//
		}
		var year = date1[0]-date2[0];
		var month = date1[1]-date2[1];
		if(month<0){
			year = year - 1;
			month = month+12;
		}
    var minuend_date = new Date(date1[0]-year, date1[1]-month, date1[2]);
    var subtrahend_date = new Date(date2[0], date2[1], date2[2]);
		var difference_days = parseInt((minuend_date-subtrahend_date)/86400000);
		if(difference_days<0){
			month = month - 1;
			difference_days=difference_days+daysInMonth(date1[0], date1[1]);
		}
		//get the next leap day after date1
		//see if its before date2
		//if it is add 1 day
		//repeat
    return [year, month, difference_days];
	}
	
	if(this.date_comparer(date_arr)<0){
		
		return subtractDates(date_arr, this.date);
	} else {
		
		return subtractDates(this.date, date_arr);
	}
}

date_calc.prototype.day_subtractor = function(date_arr){
  //need a date comparator
  function subtractDays(date1, date2){
    var minuend_date = new Date(date1[0], date1[1], date1[2]);
    var subtrahend_date = new Date(date2[0], date2[1], date2[2]);
    var difference_days = parseInt((minuend_date-subtrahend_date)/86400000);
    return difference_days;
  }
  
  if(this.date_comparer(date_arr)<0){
    return subtractDays(date_arr, this.date);
  } else {
    return subtractDays(this.date, date_arr);
  }
}

date_calc.prototype.date_comparer = function(b_arr){
	function compareDates(a_arr, b_arr) {
  	if (a_arr[0]<b_arr[0]) {
  	  return -1;
  	}
  	if (a_arr[0]===b_arr[0]&&a_arr[1]<b_arr[1]) {
  	  return -1;
  	}
  	if (a_arr[0]===b_arr[0]&&a_arr[1]===b_arr[1]&&a_arr[2]<b_arr[2]) {
  	  return -1;
  	}
  	if (a_arr[0]>b_arr[0]) {
  	  return 1;
  	}
  	if (a_arr[0]===b_arr[0]&&a_arr[1]>b_arr[1]) {
  	  return 1;
  	}
  	if (a_arr[0]===b_arr[0]&&a_arr[1]===b_arr[1]&&a_arr[2]>b_arr[2]) {
  	  return 1;
  	}
  	// a must be equal to b
  	return 0;
  }
  
  return compareDates(this.date, b_arr);
}

//console.log(new date_calc([2021, 1, 28]).date_subtractor([2020, 1, 28]));
//console.log(new Date().getMonth());


if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = date_calc;
} else {
	window.date_calc = date_calc;
}