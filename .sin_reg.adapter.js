function adaptSinRegToReact(data, args, component){
	//var data = this.state.data; //same
	var getMLCoefficients = args.getMLCoefficients;
	var printSinWeek = args.printSinWeek;

	var input_data = data.slice(0,8); //new but backwards compatable
	var points_arr = input_data.map((item, item_index)=>{
		var x = item_index+1;
		return {x: x, y: item}
	}); //same
	var coefficients = getMLCoefficients(points_arr); //same
	var output = printSinWeek(coefficients, data); //same
	component.setState({output: output, coefficients: coefficients}); //same	
	return output;
}
