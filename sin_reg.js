function sin_reg(){};

sin_reg.prototype.getMLCoefficients = function(points_arr){
	//helper functions
	function least_square_error(points_arr, a1, a0, b1, b0){
		var S = 0;
		points_arr.map(function(point, point_index){
			var x = point.x;
			var y = point.y;
			S += Math.pow(y-Math.sin(a1*x-a0)-Math.sin(b1*x-b0), 2);
		});
		return S;	
	}
	//start here
	var a1 = null;
	var a0 = null;
	var b1 = null;
	var b0 = null;
	var S = Number.POSITIVE_INFINITY;
	for (var j2 = 0; j2 < 20; j2++) {
		for (var i2 = 1; i2 < 80; i2++) {
			for (var j = 0; j < 20; j++) {
				for (var i = 1; i < 80; i++) {
					var test = least_square_error(points_arr, i/10, j/10, i2/10, j2/10);
					if(test<S){
						S = test;
						a1 = i/10;
						a0 = j/10;
						b1 = i2/10;
						b0 = j2/10;
					}
					//console.log(S);
				};
			}
		}
	}
	return {a1: a1, a0: a0, b1: b1, b0: b0, S: S};
}

sin_reg.prototype.printSinWeek = function(output_obj, data){
	//helper functions
	function getMLFunc(radians, radius, a1, a0, b1, b0) {
	  return radius*Math.abs((Math.sin(a1*radians-a0) + Math.sin(b1*radians-b0)));
	}
	function activationFunc(theta){
		if(theta>0.5) return 1
		return 0
	}
	//start here
	var output_data = [];
	data.map((data_item, data_index)=>{
		output_data.push(activationFunc(getMLFunc(data_item, 1, output_obj.a1, output_obj.a0, output_obj.b1, output_obj.b0)));
	});
	return output_data;
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = sin_reg;
} else {
	window.sin_reg = sin_reg;
}